import express from 'express';
import bodyParser from 'express';

import { 
    getLots, 
    getTradingLots,
    getAuctionsAsBidder, 
    getAuctionsAsOrganizer, 
    getLot, 
    getAuctionDetails, 
    getBidsAsBidder,
    Auction,
    startAuction,
    Bid,
    bid,
    Reveal,
    reveal,
    withdraw,
    Transfer,
    tranfer,
    getBalance,
    toHash,
    getAuctions,
    getCurrentHeight
 } from './utils/api'
const app = express();

app.use(bodyParser.json());

app.get('/:account/balance', async (req, res) => {
    const accountId = req.params.account;
    const rs = await getBalance(accountId);
    res.send(rs);
});

app.get('/:account/assets', async (req, res) => {
    const accountId = req.params.account;
    const rs = await getLots(accountId);
    res.send(rs)
});

app.get('/:account/assets/trading', async (req, res) => {
    const accountId = req.params.account;
    const rs = await getTradingLots(accountId);
    res.send(rs)
});

app.get('/assets/:asset', async (req, res) => {
    const assetId = req.params.asset;
    const rs = await getLot(assetId);
    res.send(rs)
});

app.get('/:account/auctions/organizer', async (req, res) => {
    const accountId = req.params.account;
    const rs = await getAuctionsAsOrganizer(accountId);
    res.send(rs)
});

app.get('/:account/auctions/bidder', async (req, res) => {
    const accountId = req.params.account;
    const rs = await getAuctionsAsBidder(accountId);
    res.send(rs)
});

app.get('/auctions', async (req, res) => {
    const rs = await getAuctions();
    res.send(rs)
});

app.get('/auctions/:auction', async (req, res) => {
    const auctionId = req.params.auction;
    const rs = await getAuctionDetails(auctionId);
    res.send(rs)
});

app.post('/auctions/:auction/bid', async(req, res) => {
    const auctionId = req.params.auction;
    const auction = req.body as Bid;
    auction.auctionId = auctionId;

    console.log('== Bid auction')
    console.log(JSON.stringify(auction))

    bid(auction, tx => {
        console.log('== Bid auction: TX')
        res.send(tx);
        return Promise.resolve(tx);
    })
});

app.post('/hash', async(req, res) => {
    const auction = req.body as { amount: number };

    console.log('== Hash')
    console.log(JSON.stringify(auction))

    const hashedBid = toHash(auction.amount);
    console.log('== Hash result: ' + JSON.stringify(hashedBid))

    res.send(hashedBid);
});

app.get('/height', async(req, res) => {
    const rs = await getCurrentHeight();
    res.send({
        height: rs
    });
});

app.post('/auctions/:auction/withdraw', async(req, res) => {
    const auctionId = req.params.auction;
    console.log('== Withdraw auction')

    
    withdraw(auctionId, tx => {
        console.log('== Withdraw auction: TX')
        res.send(tx);
        return Promise.resolve(tx);
    })
});

app.post('/auctions/:auction/reveal', async(req, res) => {
    const auctionId = req.params.auction;
    const auction = req.body as Reveal;
    auction.auctionId = auctionId;

    console.log('== Reveal auction')
    console.log(JSON.stringify(auction))

    reveal(auction, tx => {
        console.log('== Reveal auction: TX')
        res.send(tx);
        return Promise.resolve(tx);
    })
});

app.post('/auctions', async(req, res) => {
    const auction = req.body as Auction;

    console.log('== Start auction')
    console.log(JSON.stringify(auction))

    startAuction(auction, tx => {
        console.log('== Start auction: TX')
        res.send(tx);
        return Promise.resolve(tx);
    })
});

app.post('/transfers', async(req, res) => {
    const auction = req.body as Transfer;

    console.log('== Transfer')
    console.log(JSON.stringify(auction))

    tranfer(auction, tx => {
        console.log('== Transfer: TX')
        res.send(tx);
        return Promise.resolve(tx);
    })
});

app.get('/:account/bids', async (req, res) => {
    const accountId = req.params.account;
    const rs = await getBidsAsBidder(accountId);
    res.send(rs)
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));
