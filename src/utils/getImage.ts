import { IAuctionDetails } from './api';
import { getLot } from './api';

export async function getImage(item: IAuctionDetails): Promise<string> {
  return getLot(item.lot_assetId || '').then((lot) => lot.description);
}
