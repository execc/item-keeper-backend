import { wavesItemsApi } from '@waves/waves-games'

const api = wavesItemsApi('T');
const seed = 'scatter debris winter grid smile run erupt cube senior crunch slush depend organ floor pulse'
const seed2 = 'toilet decade kick ready access merge skull achieve state visual diary labe'
const seed3 = 'inch throw duty hand shiver potato bean elder mail under few rural essay velvet hunt'
async function createItem() {
    const items = wavesItemsApi('T') //testnet, use 'W' for mainnet
    const item = await items
      .createItem({
        version: 1,
        quantity: 1,
        name: 'Mister Meowish the Smart',
        imageUrl: 'https://s3.eu-central-1.amazonaws.com/storage.item.market/items/2g7ZrwxzTDi4X5MBZLo7WybjN8Z4srWy9dMyA8yc42TR/968a6c1c-9ecf-44b4-8780-4f24af037a34',
        misc: {
          btc: 38,
          eth: 17,
          ltc: 2
        },
      }).broadcast(seed3)
    console.log(item)
  }
  createItem()

